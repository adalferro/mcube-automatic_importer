const jsforce = require('jsforce');
const fs = require('fs');
const casefs = require('fs');
const csv = require('csv');
let parse = require('csv-parse');
const https = require('https');
require('dotenv').config();
let CSV = require('csv-string');
let accessToken;
let importData;


const {LOGIN_URL, USERNAMESalesforce, PASSWORD, TOKEN} = process.env

const conn = new jsforce.Connection({
    loginUrl:LOGIN_URL,
    //accessToken: '00Dw0000000mTKn!AR0AQGOC_zDXDVPxcYxGBGyjqfiImlyPRvbLdepK3O1bSvHyhJCjugids_mxjMWHWH2rkrmFjgWbjI9vi58fs1azJOLI822Y',
    instanceURL : LOGIN_URL
});
console.log("USERNAME",USERNAMESalesforce);
conn.login(USERNAMESalesforce, PASSWORD, (err,userInfo) => {
    if (err) {
      console.error(err);
    } else {
      loggedUser=userInfo.id;
      accessToken=conn.accessToken;
      console.log('Access token: ' + conn.accessToken);
      console.log('InstanceURL: ' + conn.instanceUrl);
      console.log('User logged:' + userInfo.id);
      console.log('Organization ID:' + userInfo.organizationId);

      //import/update ALI da csv

      let datacsv = fs.createReadStream('updateali.csv');
      conn.bulk.pollTimeout = 25000;
      conn.bulk.load('Ali__c','update',datacsv,(err,rets) => {
          if (err) { return console.error(err);}
          for (let i=0; i<rets.length; i++) {
            if(rets[i].success) {
              console.log("elemento numero "+(i+1)+" importato correttamente. id= "+rets[i].id);
            } else {
              console.log("elemento numero "+(i+1)+" non importato. Errore "+rets[i].errors);
            };
          };
          console.log("test");



          /*fs.readFileSync('createinstallation.csv', function (err, fileData) {
            parse(fileData, {columns: false, trim: true}, function(err, rows) {
              console.log('createInstallationData',rows);
            })
          })*/
          const dataarray=[];
          fs.createReadStream('createinstallation.csv')
              .pipe(csv.parse())
              .pipe(csv.transform(function(record){
                  //,Project__c,RecordTypeId,Store_Case__c,Subject,Status,OwnerId,Ali__c
                  console.log('xxx',record[0],record[1]);
                  dataarray.push(record);
                      return record;
                  //return record.map(function(value){
                  //    console.log('cella',value);
                  //    return value.toUpperCase()
                  //});
                }))
              .pipe(csv.stringify())
              //.pipe(process.stdout)
              .on("finish",function() {
                    console.log("here",dataarray);

                    dataarray.slice(1).map(caseToCreate =>createInstallation(caseToCreate))
                });
              //.pipe(createInstallation
              //)

          //parseSimple();



    
      });
    };
  });
  //batch.retrieve per i risultati dell'import

function parseSimple(){
    const output = []

    parse(fs.createReadStream('createinstallation.csv'))
        .on('readable', function(){
            let record;
            console.log('installation now!!!');
            while (record = this.read()) {
                output.push(record)
            }
        }).on('end', function(){
        console.log("xxxxxxxxxxxx"+accessToken);
        console.log(importData);


        createInstallation();

        assert.deepStrictEqual(
            output,
            [
                [ '1','2','3' ],
                [ 'a','b','c' ]
            ]
        )
    }).on('finish',function(){
        console.log("xxxxxxxxxxxx"+accessToken);});
}

function createInstallation( dataC) {
    console.log('createInstallation',dataC);
    const caseToSend={
        'Brand__c':dataC[0],
        'Project__c':dataC[1],
        'RecordTypeId':dataC[2],
        'Store_Case__c':dataC[3],
        'Subject':dataC[4],
        'Status':dataC[5],
        'OwnerId':dataC[6],
        'Ali__c':dataC[7]
    };
    const options = {
        hostname: 'mcube--staging.my.salesforce.com',
        path: '/services/data/v52.0/sobjects/Case',
        method: 'POST',
        headers: {
            'Authorization':'Bearer '+accessToken,
            'Content-Type':'application/json'
        }
    };

    const req = https.request(options, res => {
        console.log(`statusCode: ${res.statusCode}`);
        res.on('data', d => {
            process.stdout.write(d)
        });
        res.on('end', d => {
            console.log("ho FINITO!!!");
        });
    });

    req.on('error', error => {
        console.error(error)
    });

    req.write(JSON.stringify(caseToSend));
    req.end();
    console.log('ciao');
}